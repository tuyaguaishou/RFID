/*********************************************************************
			
**********************************************************************
实验序列：
实验任务: 	
实现现象：
硬件接线：
注意事项：(1) 板载晶振为12MHz，波特率为2400、4800、7200均可找到误差不太
			  大的设置值，而有些波特率如9600则无法找到误差小的设置值，
			  因此不要设置9600这类波特率。
		  (2) 整个串口的代码和波特率计算，都可以由单片机小精灵工具自动
		      计算和生成，但是自己要理解计算过程，要能结合数据手册看懂
			  代码是怎么回事，不能只知其然不知其所以然
		  (3) 注意上位机串口助手的设置参考本文件夹中图“8.1.串口助手设置.png”
		  (4) 因为上位机中串口不能被2个程序同时打开，因此在stcisp软件下载
		      程序前必须将串口助手中串口关闭，否则一旦串口助手中打开串口
			  情况下进行下载，则stcisp软件会提示下载串口打开失败
		  
版    本：
作    者：
时	  间：										  
**********************************************************************/


/*
**********************************************************************
*                         头文件包含
**********************************************************************
*/
#include <reg52.h>
#include "main.h"
#include "rc522.h"
#include<string.h>
#include <eeprom.h>
/*
**********************************************************************
*                         本地全局变量
**********************************************************************
*/
	
u8 UID[5],Temp[4];
u16 count = 0, time=0;

u8 RUID[3][4] = {0};
void Rc522Test1(void);
void Rc522Test2(void);
void wait(void);
void w_dat(unsigned char dat);
void w_cmd(unsigned char cmd);
void Init_LCD1602(void);
sbit BF = P0^7;
sbit EN = P2^5;
sbit RS = P1^0;
sbit RW = P1^1;

sbit beep = P2^3;
sbit K_1 = P3^4;
sbit K_2 = P3^5;
sbit K_3 = P3^6;
unsigned char M = 9;
unsigned char i;
unsigned char money[10] = {'9','8','7','6','5','4','3','2','1','0'};
void regist_clean();
void Shibie();
/*********************************************************************
* 函 数 名       : main
* 函数功能		 : 主函数
* 参数列表       : 无
* 函数输出    	 : 无
*********************************************************************/

void main(void)
{
	SysInit();
	Init_LCD1602();
	
    w_cmd(0x80);
	w_dat('N');
	w_cmd(0x81);
	w_dat('U'); 
	w_cmd(0x82);
	w_dat('M'); 
	w_cmd(0x83);
	w_dat(':'); 


	while(1){
		regist_clean();
		Shibie();
	}	
}



/*********************************************************************
* 函 数 名       : delay1ms
* 函数功能		 : 延时
* 参数列表       : 无
* 函数输出    	 : 无
*********************************************************************/
void delay1ms(unsigned int i)   //误差 0us,括号内填什么数字就是多少毫秒
{
    unsigned char a,b,c;
	for(;i>0;i--)
 	   for(c=1;c>0;c--)
    	    for(b=142;b>0;b--)
        	    for(a=2;a>0;a--);
}



void regist_clean(){
	int R_1 = 0,R_2 = 0;
	if(K_1 == 0)//登记
	{
		delay1ms(100);//防止抖动
		if(K_1 == 0){
			w_cmd(0x01);
			if (PcdRequest(0x52, Temp) == MI_OK)
	   		{
	   			if (PcdAnticoll(UID) == MI_OK)
				{
				    w_cmd(0xc0);
					w_dat('W');
					w_cmd(0xc1);
					w_dat('I');
					w_cmd(0xc2);
					w_dat('T');
					
					for(R_1 = 0;R_1<8 ;R_1++){
						if(dcx(0x2000+R_1*0x200) == 0xff&&dcx(0x2000+R_1*0x200+1) == 0xff&&dcx(0x2000+R_1*0x200+2) == 0xff&&dcx(0x2000+R_1*0x200+3) == 0xff){
							/****卡号**/
							xcx(0x2000+R_1*0x200,UID[0]);
							xcx(0x2000+R_1*0x200+1,UID[1]);
							xcx(0x2000+R_1*0x200+2,UID[2]);
							xcx(0x2000+R_1*0x200+3,UID[3]);
							xcx(0x2000+R_1*0x200+4,money[9]);
							xcx(0x2000+R_1*0x200+5,9);
							w_cmd(0xc0);w_dat('W');
							w_cmd(0xc1);w_dat('I');
							w_cmd(0xc2);w_dat('T');
							w_cmd(0xc3);w_dat('O');
							w_cmd(0xc4);w_dat(' ');
							w_cmd(0xc5);w_dat('K');
							delay1ms(500);
							w_cmd(0x01);
							beep=0;
							delay1ms(100);
							beep=1;
							break;
						}
					}	
				}		
			}
		}		
	}

	if(K_2 == 0)	//消除
	{
		delay1ms(100);
		if(K_2 == 0){
			w_cmd(0x01);
			if (PcdRequest(0x52, Temp) == MI_OK)
		   	{
		   		if (PcdAnticoll(UID) == MI_OK)
				{
					w_cmd(0xc0);
					w_dat('C');
					w_cmd(0xc1);
					w_dat('L');
					w_cmd(0xc2);
					w_dat('E');
					for(R_2 = 0;R_2<8 ;R_2++){
						if(dcx(0x2000+R_2*0x200) == UID[0]&&dcx(0x2000+R_2*0x200+1) == UID[1]&&dcx(0x2000+R_2*0x200+2) == UID[2]&&dcx(0x2000+R_2*0x200+3) == UID[3]){
							cc(0x2000+R_2*0x200);
							w_cmd(0xc0);w_dat('C');
							w_cmd(0xc1);w_dat('L');
							w_cmd(0xc2);w_dat('E');
							w_cmd(0xc3);w_dat('O');
							w_cmd(0xc4);w_dat(' ');
							w_cmd(0xc5);w_dat('K');
							delay1ms(500);
							w_cmd(0x01);
							beep=0;
							delay1ms(100);
							beep=1;
							break;
						}		
					}
				}
			}
		}
	}
	if(K_3 == 0){
	   delay1ms(100);
	   if(K_3 == 0){
	   		cc(0x2000);
			cc(0x2200);
			cc(0x2400);
			cc(0x2600);
			cc(0x2800);
			cc(0x2A00);
			cc(0x2C00);
			cc(0x2E00);
			w_cmd(0xc0);w_dat('O');
			w_cmd(0xc1);w_dat('K');
			beep=0;
			delay1ms(100);
			beep=1;
			delay1ms(100);
			beep=0;
			delay1ms(100);
			beep=1;
			delay1ms(100);
			beep=0;
			delay1ms(100);
			beep=1;
			w_cmd(0x01);
	   }
	
	}
}

void Shibie(){
	int R_2 = 0;
	if (PcdRequest(0x52, Temp) == MI_OK)
	{
	   	if (PcdAnticoll(UID) == MI_OK)
		{
			w_cmd(0x80);
			w_dat('C');
			w_cmd(0x81);
			w_dat('A');
			w_cmd(0x82);
			w_dat('R');
			w_cmd(0x83);
			w_dat('D');
			for(R_2 = 0;R_2<8 ;R_2++){
				if(dcx(0x2000+R_2*0x200) == UID[0]&&dcx(0x2000+R_2*0x200+1) == UID[1]&&dcx(0x2000+R_2*0x200+2) == UID[2]&&dcx(0x2000+R_2*0x200+3) == UID[3]){
					w_cmd(0x84);w_dat(UID[0]);
					w_cmd(0x85);w_dat(UID[1]);
					w_cmd(0x86);w_dat(UID[2]);
					w_cmd(0x87);w_dat(UID[3]);
					/*扣费*/
					
					xcx(0x2000+R_2*0x200+4,money[M]);
					
					w_cmd(0x8e);w_dat(money[M]);
					
					/****记录次数**/	
					/*******************/
					M--;
					if(M == 0){
						M = 9;
						beep = 0;
						delay1ms(100);
						beep = 1;
						delay1ms(100);
						beep = 0;
						delay1ms(100);
						beep = 1;
						delay1ms(100);
						cc(0x2000+R_2*0x200);
						
					}
					w_cmd(0xC5);w_dat('L');
					w_cmd(0xC6);w_dat('O');
					w_cmd(0xC7);w_dat('A');
					w_cmd(0xC8);w_dat('D');
					w_cmd(0xC9);w_dat('I');
					w_cmd(0xCA);w_dat('N');
					w_cmd(0xCB);w_dat('G');

					delay1ms(10000);

					w_cmd(0xC0);w_dat('O');
					w_cmd(0xC1);w_dat('K');
					delay1ms(500);
					
					
					
					w_cmd(0x01);
					break;		
				}		
			}
		}
	}
}


/*********************************************************************
* 函 数 名       : SysInit
* 函数功能		 : 串口初始化为2400波特率，寻卡，读写卡数据
* 参数列表       : 无
* 函数输出    	 : 无
*********************************************************************/
void SysInit(void)
{
    TMOD = 0x21;		// T1设置为8位自动重装载定时器			
    SCON = 0x40;		// 串口工作在模式1：8位UART波特率可变，且禁止接收
    TH1 = 0xE8;			// 单片机小精灵V1.3算出的2400波特率且波特率
    TL1 = TH1;			// 加倍时的定时器设置值。
    PCON = 0x80;		// 设置为波特率加倍
	EA = 1;				// 开总中断
	ES = 1;			    // 开串口中断
	TR1 = 1;		    // 定时器1开启计数


    PcdReset();				//复位RC522
    PcdAntennaOff(); 	 	//关天线 		
    PcdAntennaOn();  		//开天线
	M500PcdConfigISOType('A');
}



/***************************************************/
//是否空闲
void wait(void)
{
	 P0=0xFF;
	
	 do
		 {
		 RS=0;
		 RW=1;
		 EN=0;
		 EN=1;
		 }
	 while (BF==1);
	 EN=0;
}

//写数据函数
void w_dat(unsigned char dat)
{
	wait();
	EN=0;
	P0=dat;
	RS=1;
	RW=0;
	EN=1;
	EN=0;
}
//写指令
void w_cmd(unsigned char cmd)
{
	wait();
	EN=0;
	P0=cmd;
	RS=0;
	RW=0;
	EN=1;
	EN=0;
}
//初始
void Init_LCD1602(void)
{
	w_cmd(0x38);
	w_cmd(0x0c);
	w_cmd(0x06);
	w_cmd(0x01);
}
/************************************************/