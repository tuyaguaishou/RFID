#ifndef __EEPROM_H__
#define __EEPROM_H__

#define uchar unsigned char   
#define uint unsigned int
sfr ISP_DATA = 0xe2;   
sfr ISP_ADDRH = 0xe3;     
sfr ISP_ADDRL = 0xe4;   
sfr ISP_CMD = 0xe5;   
sfr ISP_TRIG = 0xe6;      
sfr ISP_CONTR = 0xe7;
sbit LED1 = P1^0;
sbit LED2 = P1^1;
sbit K1   = P3^4;                           //��ť1
sbit K2   = P3^5;  
void  cc(uint addr);
void  xcx(uint addr,uchar dat);
uchar dcx(uint addr);
void  Q0();


#endif